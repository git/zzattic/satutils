#!/usr/bin/perl

=head1 NAME

SatUtils::TelnetInterface - base class for telnet interfaced devices

=head1 DESCRIPTON

This is a base class for communication with devices that have a telnet
based command-line interface.

=cut

package SatUtils::TelnetInterface;

use fields qw(conn prompt promptrx atprompt);

use warnings;
use strict;
use IO::Socket::INET;

=head1 METHODS

=over 4

=item new

Opens a connection to the device. Pass it the address of the device, the
port to connect to, and regex to use to regognise the device's command
prompt, and a regex to use to recognise the prompt.

Returns an object representing this connection.

=cut

sub new {
	my SatUtils::TelnetInterface $this = shift;
	my $address=shift;
	my $port=shift;
	my $prompt=shift;
	my $promptrx=shift;
	
	$this=fields::new($this) unless ref $this;
	$this->{prompt} = $prompt;
	$this->{atprompt} = 0;
	$this->{conn} = IO::Socket::INET->new(
		PeerHost => $address,
		PeerPort => $port,
		Blocking => 0,
	);

	return $this;
}

=item getoutput

Get output of previous command until next prompt, and return it as one
string.

=cut

sub getoutput {
	my $this=shift;
	
	my $ret='';
	
	while (! $this->{atprompt}) {
		if (! $this->{conn}->read($_, 128)) {
			select(undef, undef, undef, 0.025);
			next;
		}

		$ret.=$_;
		if ($ret=~/$this->{prompt}/) {
			$this->{atprompt}=1;
			$ret=~s/$this->{prompt}//;
		}
	}

	$ret=~s/\r//g;
	return $ret;
}

=item sendcommand

Ensure we're at a prompt line, and send the passed command.

=cut

sub sendcommand {
	my $this=shift;
	my $command=shift;
	
	$this->getoutput;
	$this->{conn}->print($command."\r\n");
	$this->{atprompt}=0;
}

=back

=head1 AUTHOR

Joey Hess <joey@kitenet.net>

=cut

1
