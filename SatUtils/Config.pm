#!/usr/bin/perl

=head1 NAME

SatUtils::Config - config file parser for satutils

=head1 DESCRIPTON

Reads /etc/satutils.cfg and ~/.satutilsrc for configuration. These files are
typical key=value files with hash comments.

=cut

package SatUtils::Config;
use warnings;
use strict;

our %config = (
	d2address => "192.168.1.250",
	dw6000address => "192.168.0.1",
	dw6000port => 1953,
);
our $readconfig=0;

=head1 METHODS

=over 4

=item readconfig

Read a given config file, does not fail if the file does not exist.

=cut

sub readconfig {
	my $file=shift;
	return unless -e $file;
	my $fh;
	open($fh, $file) || die "read $file: $!";
	while (<$fh>) {
		chomp;
		next unless length;
		next if /^#/;
		my ($key, $value)=split('=', $_, 2);
		$config{$key}=$value if defined $key && defined $value;
	}
	close $fh;
}

=item get

Reads config files on first call. 
Gets a given config value. Pass the key to get.

=cut

sub get {
	my $key=shift;
	
	if (! $readconfig) {
		$readconfig=1;
		readconfig("/etc/satutils.cfg");
		readconfig("$ENV{HOME}/.satutilsrc");
	}

	return $config{$key};
}

=back

=head1 AUTHOR

Joey Hess <joey@kitenet.net>

=cut

1
