#!/usr/bin/perl

=head1 NAME

SatUtils::DW6000 - interface to DW6000 VSAT Modem

=head1 SYNOPSIS

 use SatUtils:DW6000;
 my $m=SatUtils::DW6000->new;

=head1 DESCRIPTON

This is an interface to the DW6000 VSAT Modem. It can be used to extract
some values from the modem, as well as reboot it and do a few other useful
things. It's fairly limited so far.

=cut

package SatUtils::DW6000;
use warnings;
use strict;
use base 'SatUtils::TelnetInterface';
use fields;
use SatUtils::Config;

=head1 METHODS

=over 4

=item new

Opens a connection to the DW6000. By default connects to an IP
address as defined by dw6000address in the config file, or a reasonable
default, but a parameter can be passed with an IP address or hostname of
the controller to connect to.

Returns an object representing this connection.

Note that the DW6000 only supports one connection at a time, and blocks
other connections until the current one is closed.

=cut

sub new {
	my $this = shift;
	my $address=shift;
	$address=SatUtils::Config::get('dw6000address')
		unless defined $address && length $address;
	my $port=SatUtils::Config::get('dw6000port');
	
	$this=fields::new($this) unless ref $this;
	$this->SUPER::new($address, $port, qr#.* Menu (.*): $#);
	return $this;
}

=item reboot

Reboots the DW6000.

=cut

sub reboot {
	my $this=shift;
	
	$this->sendcommand("rr");
	$this->{conn}=undef;
	# no result
}

=item getvalues

Returns a hash of values from the DW6000. Currently limited to values from the satelite
interface stats main statistics page.

=cut

sub getvalues {
	my $this=shift;

	$this->sendcommand("c");
	$this->sendcommand("a");
	my $s=$this->getoutput;
	$this->sendcommand("z"); # back to main menu

	my %ret;
	while ($s=~m/(?:^|   +)(.*?)\.\.+ (.*?)(?=$|   )/mg) {
		$ret{$1}=$2;
	}
	return %ret;
}

=item DESTROY

The DW6000's telnet interface is fragile; leaving it without properly
existing can hang the device hard. This module uses a DESTROY hook 
to try to always shut down connections cleanly.

=cut

sub DESTROY {
	my $this=shift;

	if (defined $this->{conn}) {
		$this->sendcommand("q");
		$this->{conn}->close;
	}
}

=back

=head1 BUGS

Interrupting a program that uses this module in the middle of a command
canhang the DW6000.

=head1 AUTHOR

Joey Hess <joey@kitenet.net>

=cut

1
