#!/usr/bin/perl

=head1 NAME

SatUtils::D2 - interface to DataStorm D2 mobile satelite internet controller

=head1 SYNOPSIS

 use SatUtils::D2;
 my $s=SatUtils::D2->new;
 print $s->getvalue("sysstatus")."\n";

=head1 DESCRIPTON

This is an interface to the DataStorm D2 mobile satelite internet
controller. It can be used to get values from the controller, as well as
send the controller other commands through its telnet interface and get the
results of those commands.

=cut

package SatUtils::D2;
use warnings;
use strict;
use base 'SatUtils::TelnetInterface';
use fields;
use SatUtils::Config;

=head1 METHODS

=over 4

=item new

Opens a connection to the D2 controller. By default connects to an IP
address as defined by d2address in the config file, or a reasonable
default, but a parameter can be passed with an IP address or hostname of
the controller to connect to.

Returns an object representing this connection.

=cut

sub new {
	my $this = shift;
	my $address=shift;
	$address=SatUtils::Config::get('d2address')
		unless defined $address && length $address;
	
	$this=fields::new($this) unless ref $this;
	$this->SUPER::new($address, "telnet", qr/DATASTORM % $/);

	return $this;
}

=item listvalues

Returns a hash of known available values, where the key is the value and
the hash value is a description of it. Note that this is currently a hardcoded,
non-exhaustive list.

=cut

sub listvalues {
	my $this=shift;

	return (
 		longitude => "gps",
		latitude => "gps",
 		altitude => "feet above sea level",
		velocity => "what are the units?",
		heading	=> "",
		truecompass => "degrees",
		magcompass => "degrees",
		current	=> "amp",
		temp => "farenheight; measured where?",
		sysstatus => "system status",
		txstatus => "tranmit status",
		rxstatus => "receive status",
		signalquality => "",
		signalstrength => "",
		satel => "satellite elevation",
		sattrueaz => "satellite true azimuth",
		satlon => "satellite longitude",
		satmagaz => "satellite magnetic azimuth",
		dishel => "target dish elevation",
		dishaz => "target dish azimuth",
		dishsk => "target dish skew",
		skangle => "current dish skew",
		skcount => "skew count",
		azangle => "current dish azimuth",
		azcount => "azimuth count",
		elangle => "current dish elevation",
		elcount => "elevation count",
		gpscolor => "color of GPS LED",
		busycolor => "color of busy LED",
		readycolor => "color of ready LED",
		stowcolor => "color of stow LED",
		lancolor => "color of LAN LED",
		lnbcolor => "color of LNB LED",
		eltilt => "elevation tilt",
		sktilt => "skew tilt",
		license => "",
		satpolar => "satellite polarization",
		dishlight => "dish light",
	);
	#Values I'm still looking for include:
	#number of visible gps satellites
}

=item getvalue

Use the getvalue command to get and return a single value.

=cut

sub getvalue {
	my $this=shift;
	my $value=shift;
	
	$this->sendcommand("getvalue $value");
	my $ret=$this->getoutput;
	chomp $ret;
	$ret=~s/.* = "(.*)".*/$1/;
	return $ret;
}

=back

=head1 AUTHOR

Joey Hess <joey@kitenet.net>

=cut

1
